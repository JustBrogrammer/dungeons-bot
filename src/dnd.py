import math
import requests_cache
import random
from enum import Enum
import os
import util

# URL where the API is available.
# All requests will be prefixed with /api/
BASE_URL = "https://www.dnd5eapi.co"
# Experience requirement to reach each level.
# index 0 = level 1, index 1 = level 2, etc.
EXPERIENCE_REQUIREMENT = [
    0, 300, 900, 2700,              # 1-4    (prof 2)
    6500, 14000, 23000, 34000,      # 5-8   (prof 3)
    48000, 64000, 85000, 100000,    # 9-12  (prof 4)
    120000, 140000, 165000, 195000, # 13-16 (prof 5)
    225000, 265000, 305000, 355000  # 17-20 (prof 6)
]
# Currency units valued by copper pieces
EXCHANGE_RATES = {
    "cp": 1,
    "sp": 10,
    "ep": 50,
    "gp": 100,
    "pp": 1000
}
# Path to cache requests (with no file extension)
CACHE_PATH = "data/cache"

session: requests_cache.CachedSession = None

class AttackType(Enum):
    """
    Type of attack.

    `>0`: hit
    `=0`: no attack
    `<0`:  miss
    """

    CRIT_FAILURE = -2
    MISS = -1
    NONE = 0
    NORMAL = 1
    CRIT_SUCCESS = 2


class AttackResult():
    """
    Represents an attack result, with a damage amount, crit, and name.
    """

    def __init__(self, damage: int, attack_type: AttackType, name: str) -> None:

        self.damage = damage
        self.attack_type = attack_type
        self.name = name


def reset_cache() -> None:

    global session
    if (session):
        session.cache.clear()
        session.close()
        session = None

    session = requests_cache.CachedSession(os.path.join(util.root, CACHE_PATH))


def query(path: str, params: dict = None) -> dict:

    if (not session):
        reset_cache()

    response = session.get(f"{BASE_URL}{path}", params = params)
    if (response.from_cache):
        util.debug(f"Retrieved cache for {response.url}")
    else:
        util.debug(f"Requesting {response.url}")

    if (response.status_code == 200):
        response_dict = response.json()
        return response_dict
    else:
        util.warn(f"Requesting {response.url} resulted in an invalid request.")
        return {}


def experience_level(experience_points: int) -> int:
    """
    Determine the corresponding level from the given experience points.
    """

    level = 0
    for i in range(len(EXPERIENCE_REQUIREMENT)):
        if (experience_points >= EXPERIENCE_REQUIREMENT[i]):
            level = i
        else:
            break

    return level + 1


def get_races() -> list[dict]:

    return query("/api/races")["results"]


def get_race(race_index: str) -> dict:

    return query(f"/api/races/{race_index}")


def get_classes() -> list[dict]:

    return query("/api/classes")["results"]


def get_class(class_index: str) -> dict:

    return query(f"/api/classes/{class_index}")


def get_class_level(class_index: str, level: int) -> dict:

    return query(f"/api/classes/{class_index}/levels/{level}")


def get_abilities() -> list[dict]:

    return query("/api/ability-scores")["results"]


def get_ability(ability_index: str) -> dict:

    return query(f"/api/ability-scores/{ability_index}")


def get_proficiency(prof: str) -> dict:

    return query(f"/api/proficiencies/{prof}")


def get_skills() -> list[dict]:

    return query("/api/skills")["results"]


def get_skill(skill_index: str) -> dict:

    return query(f"/api/skills/{skill_index}")


def get_proficiency_ability(proficiency_index: str) -> dict:
    """Gets the ability related to the given proficiency."""

    proficiency = get_proficiency(proficiency_index)["reference"]["index"]
    skill = get_skill(proficiency)
    if (skill):
        return get_ability(skill["ability_score"]["index"])
    else:
        util.warn(f"Proficiency {proficiency_index} isn't associated with an ability.")
        return {}


def get_equipment(equipment_index: str):

    return query(f"/api/equipment/{equipment_index}")


def get_equipment_category(category_index: str):

    return query(f"/api/equipment-categories/{category_index}")


def get_monster(monster_index: str) -> dict:

    return query(f"/api/monsters/{monster_index}")


def get_monsters(challenge_ratings: list[float] = None) -> list[dict]:

    return query("/api/monsters", {"challenge_rating": challenge_ratings})["results"]


def get_monster_of_rating(challenge_ratings: list[float] = None) -> dict:

    monsters = get_monsters(challenge_ratings)
    monster_index = random.choice(monsters)["index"]
    return get_monster(monster_index)


def get_monster_lower(challenge_rating = 0.5) -> dict:
    """Get a monster with the given `challenge_rating` or lower"""

    challenge_ratings = [0, 0.125, 0.25, 0.5][:int(challenge_rating * 8) + 1]
    challenge_ratings += [i + 1 for i in range(math.floor(challenge_rating))]
    return get_monster_of_rating(challenge_ratings)


def get_monster_image_link(monster: dict) -> str:

    link = ""
    if ("image" in monster):
        link = "{}{}".format(BASE_URL, monster["image"])

    return link.replace("www.", "")


def get_weapon_ability(weapon: dict) -> list[str]:
    """Get the abilities associated with the given weapon."""

    for property in weapon["properties"]:
        if (property["index"] == "finesse"):
            return ["str", "dex"]

    if (weapon["weapon_range"].lower() == "melee"):
        return ["str"]

    elif (weapon["weapon_range"].lower() == "ranged"):
        return ["dex"]


def exchange(unit_from: str, quantity: int, unit_to = "gp", round = True) -> float:
    """
    Exchange currency from one currency unit to another.
    If `round` is True, the resulting conversion is rounded up. Ex. 1 sp -> gp = 1 gp
    """

    exchanged = (EXCHANGE_RATES[unit_from] * quantity) / EXCHANGE_RATES[unit_to]
    if (round): exchanged = math.ceil(exchanged)
    return exchanged


def max_roll(dice: str) -> int:
    """
    Return the maximum possible roll for the given dice.
    """

    mod = 0
    strip_dice = dice

    if ("+" in dice):
        strip_dice = dice[:dice.index("+")]
        mod += int(dice[dice.index("+") + 1:])

    if ("-" in dice):
        strip_dice = dice[:dice.index("-")]
        mod += -int(dice[dice.index("-") + 1:])

    s = strip_dice.split("d")
    return (int(s[0]) * int(s[1])) + mod


def roll(*dice: str) -> list:
    """
    Roll y, z sided dice in the given format ydz.
    For example, "2d6" -> rolls 2, 6 sided dice and returns both rolls.
    Ignores + and - operators.
    """

    rolls = []

    for die in dice:
        strip_dice = ""
        if ("d" in die):
            strip_dice = die.split("+")[0].split("-")[0].split("d")
        else:
            strip_dice = ["1", die]

        for _ in range(int(strip_dice[0])):
            rolls.append(random.randint(1, int(strip_dice[1])))

    return rolls


def roll_sum(*dice: str) -> int:
    """Alias for `sum(roll(dice))`."""

    rolls_sum = 0
    min = 0
    for die in dice:
        mod = 0
        if ("+" in die):
            mod = int(die.split("+")[1])
        elif ("-" in die):
            mod = -int(die.split("-")[1])
        rolls = roll(die)
        min += len(rolls)
        rolls_sum += sum(rolls) + mod

    return max(min, rolls_sum)


def roll_min(*dice: str, t = 1) -> int:
    """
    Returns the lowest `t` rolls, added together.
    """

    return sum(sorted(roll(*dice))[:t])


def roll_max(*dice: str, t = 1) -> int:
    """
    Returns the highest `t` rolls, added togther.
    """

    rolls = sorted(roll(*dice))
    return sum(rolls[len(rolls) - t:])


def ability_modifier(ability_score: int) -> int:
    """Get the ability modifier for a given score."""

    return math.floor((ability_score - 10) / 2)


def cr_proficiency(cr: float) -> int:
    """Determine the proficiency bonus from the given challenge rating."""

    return 2 if cr == 0 else math.ceil(cr / 4) + 1
