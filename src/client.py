from time import sleep
from mastodon import Mastodon
from datetime import datetime, timezone
import util
import random
import os
import string

# Maximum number of options in a poll
MAX_POLL_SIZE = 4

# Whether to actually make posts or not
dry = False
# When dry, used for simulating poll ids
poll_id = 0
# When dry, used for keeping track of past poll options
last_poll: list[str] = []
# The last status ID that was posted
status_chain: int = None
# Mastodon client
mastodon: Mastodon = None

def init_client():

    global mastodon
    mastodon = Mastodon(access_token = os.path.join(util.root, "data/.token"))


def post_status(status_text: str, last_result: str = "", chain = True) -> dict:
    """Post a plain-text status, returning its data."""

    util.info("Posting status")
    global status_chain

    if (dry):
        global poll_id
        poll_id += 1
        status_chain = poll_id
        return {
            "id": poll_id
        }

    status_text = f"{last_result}\n---\n{status_text}" if last_result else status_text
    status = mastodon.status_post(status_text, in_reply_to_id = status_chain if chain else None)
    status_chain = status["id"]
    return status


def post_poll(status_text: str, last_result: str, options: list[str], chain = True, extend = False) -> dict:
    """Post a poll, returning its data."""

    util.info("Posting poll")
    global status_chain

    if (dry):
        global last_poll
        global poll_id
        last_poll = options
        poll_id += 1
        status_chain = poll_id
        return {
            "id": poll_id,
            "poll": {}
        }

    duration = util.config["default_poll_duration"]
    if (extend): duration *= util.config["no_vote_scale"]
    p = mastodon.make_poll(options, duration)
    status_text = f"{last_result}\n---\n{status_text}" if last_result else status_text
    status = mastodon.status_post(status_text, poll = p, in_reply_to_id = status_chain if chain else None)
    status_chain = status["id"]
    return status


def delete_post(status_id: int) -> dict:
    """Delete the given post, returning its data."""

    global status_chain

    if (dry):
        if (status_chain == status_id): status_chain = poll_id - 1
        return {
            "id": poll_id
        }

    status = mastodon.status_delete(status_id)
    if (status["id"] == status_chain): status_chain = status["in_reply_to_id"]
    return status


def poll_result(status_id: int, chain = True) -> int:
    """
    Gets poll results from the post with given ID.
    This is a blocking call and will wait until the poll is expired.
    """

    util.debug("Waiting for poll to end...")
    global status_chain

    if (dry):
        return random.randrange(0, len(last_poll))

    sleep(max(1, util.config["default_poll_duration"] - util.config["poll_check_time"]))
    status = mastodon.status(status_id)

    if ("poll" not in status):
        raise ValueError("Status does not contain a poll.")

    diff = status["poll"]["expires_at"] - datetime.now(timezone.utc)
    sleep(max(1, diff.total_seconds() - util.config["poll_check_time"]))

    # update status info
    status = mastodon.status(status_id)
    poll = status["poll"]

    util.debug("Probing poll...")

    # probe poll
    if (not poll["expired"]):
        diff = poll["expires_at"] - datetime.now(timezone.utc)
        # Poll has votes, so just wait for completion
        if (poll["votes_count"] > 0):
            sleep(max(1, diff.total_seconds() + 1))
        # No votes yet, extend poll time
        else:
            old_status = delete_post(status_id)
            new_status = post_poll(old_status["text"], "", [p["title"] for p in poll["options"]], chain, True)
            util.debug("Poll has no votes, reposting...")
            return poll_result(new_status["id"], chain)

    # gather results
    util.debug("Polling results...")
    results = poll["options"]
    winner = 0
    v = 0
    for i in range(len(results)):
        if (results[i]["votes_count"] > v):
            v = results[i]["votes_count"]
            winner = i

    return winner


def get_followers() -> list[str]:
    """Get the list of follower names."""

    if (dry):
        followers = []
        for _ in range(100):
            followers.append("".join(random.choices(string.printable, k = random.randint(1, 16))))
        return followers

    return [f["display_name"] for f in mastodon.account_followers(mastodon.me())]


def update_profile(bio: str) -> dict:
    """Updates the user profile based upon the Character description."""

    if (dry):
        return {
            "note": "This is a bio"
        }

    return mastodon.account_update_credentials(
        note = bio
    )
