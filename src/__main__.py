import argparse
import client
from campaign import Campaign
import util
import signal
import sys
from time import sleep
import dnd

def cancel(signal, frame):
    util.warn("Forcibly exiting...")
    client.post_status("Campaign forcibly stopped.")
    sys.exit(0)


parser = argparse.ArgumentParser()
parser.add_argument(
    "--dry", "-d",
    help = "Do not actually make posts.",
    const = True,
    action = "store_const"
)
parser.add_argument(
    "--no-save",
    help = "Do not perform or load saves.",
    const = True,
    action = "store_const"
)
args = parser.parse_args()

signal.signal(signal.SIGINT, cancel)

dnd.reset_cache()
campaign: Campaign = None
campaigns = 0
client.dry = args.dry
if (not args.dry):
    client.init_client()

util.info("Staring epic...")

if (not args.no_save):
    try:
        campaign = Campaign.load()
        campaigns = campaign._id
        if (campaign._version != util.config["_version"]):
            campaign = None
            util.warn("Loaded save from file, but its outdated, discarding.")
        else:
            util.info("Loaded save from file, resuming.")
    except:
        campaign = None
        util.warn("Unable to load a save, starting fresh.")

while True:
    dnd.reset_cache()

    if (not campaign):
        campaigns += 1
        campaign = Campaign(campaigns, util.config["_version"])
        campaign.start()
    else:
        campaign.start(util.emojis["prompts"]["resume"])

    campaign = None

    if (not args.dry):
        sleep(util.config["death_duration"])
