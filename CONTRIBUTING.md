# Contributing

1. Fork the repository
2. Clone your fork
3. Create a branch for your changes
4. Make your changes
5. Run tests (these tests do not offer good coverage, make sure to test yourself locally, ideally with a test Mastodon account)
    ```shell
    pytest
    ```
6. Commit and push your changes
7. Submit a Pull Request (PR) to the `main` branch of the original repository
